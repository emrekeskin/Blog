---
title: Meteor Shortcode#3 - Mongod Sıfırlama
date: 2016-10-26 23:28:01
author: Emre Keskin
desc: Mongod ayarını default ayarlarına getirme
---

### Mongod ayarını default ayarlarına getirme işlemi


```

unset MONGO_URL
meteor

```
