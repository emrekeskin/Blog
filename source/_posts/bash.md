---
title: Kısadan Bash Script 
date: 2016-12-7 14:49:13
author: Emre Keskin
desc: Kısadan Bash Script 
---

### Kısadan Bash Script 

Kısadan bash script kodu çalıştırması ve kullanımı 


### Kullanımı

```
chmod 777 ./file.sh
./file.sh

```

### Kısa Kod 

```

#!/bin/sh

ssh -T root@ipadresi << EOF

cd empatiblog
sudo fuser -k 80/tcp   
git pull git@bitbucket.org:empati/blog.git
hexo server -p 80

EOF
exit

```