---
title: Meteor EACCES permission denied Hatası
date: 2016-12-10 05:13:13
author: Emre Keskin
desc: Kısadan Bash Script 
---


HATA 
===
~~~

/Users/EmreKeskin/.meteor/packages/meteor-tool/.1.4.2_3.1rd9djy++os.osx.x86_64+web.browser+web.cordova/mt-os.osx.x86_64/dev_bundle/lib/node_modules/meteor-promise/promise_server.js:190
      throw error;
      ^

Error: EACCES: permission denied, open '/Users/EmreKeskin/Desktop/SaltanatPanel/.meteor/local/.resolver-result-cache.json.t6nlk6'

~~~


ÇÖZÜM
=====

Buna benzer bir hata aldığınızda .meteor/local dosyasını silin.

Meteor'u tekrar başlattığınızda sorun çözülecektir

Fakat bunun birçok nedenide olabilir. Yinede yedek almayı unutmayınız

