---
title: Meteor Shortcode#1 - Kalıcı Session Oluşturma
date: 2016-10-18 06:32:01
author: Emre Keskin
desc: Permanent Session Meteor
---

### Kalıcı Session Oluşturma - Permanent Session Meteor

Meteor'un kendi session sistemi aslında var. Çokta güzel çalışıyor fakat sayfa yenilendiğinde sessionlar temizleniyor bunun için bir paket kurarak çok basit şekilde client tarafında kalıcı yapabiliyoruz.

### Paket

```

meteor add u2622:persistent-session

```


### Kullanımı

> - Set

```javascript

Session.setPersistent('variableName', value);

```

> - Get

```javascript

Session.get('variableName');

```

> - Kütüphane'nin Diğer Özellikleri

[on Github](https://github.com/okgrow/meteor-persistent-session)
[on AtmosphereJS](https://atmospherejs.com/u2622/persistent-session)
