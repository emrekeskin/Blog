---
title: Meteor Shortcode#4 - Mysql
date: 2016-12-07 06:32:01
author: Emre Keskin
desc: Mysql
---

### Mysql 


```javascript

import squel from "squel"
import mysql from "mysql"

var account = mysql.createConnection({
  host     : 'ipadresi',
  user     : 'root',
  password : 'password',
  database : 'account'
});


account.connect();

Object.values = x => Object.keys(x).map(w=>x[w])

AccountInsert = function(t,s) {
  var e = Object.values(s).map(x=>JSON.stringify(x)).join(",");
  s = Object.keys(s).join(",");
  account.query("INSERT INTO "+t+" ("+ s +") VALUES ("+e +")")
}


Accounts.query('SELECT * FROM pages WHERE id = 1', function(err, rows, fields) {
  if (err) throw err;

  console.log('The solution is: ', rows[0].solution);
});
 });


```